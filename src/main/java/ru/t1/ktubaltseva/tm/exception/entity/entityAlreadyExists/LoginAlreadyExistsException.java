package ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists;

public final class LoginAlreadyExistsException extends AbstractAlreadyExistsException {

    public LoginAlreadyExistsException() {
        super("Error! Login already exists...");
    }

    public LoginAlreadyExistsException(String message) {
        super("Error! Login \"" + message + "\" already exists...");
    }
}
