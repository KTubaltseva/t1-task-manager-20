package ru.t1.ktubaltseva.tm.api.service;

import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.field.AbstractFieldException;
import ru.t1.ktubaltseva.tm.exception.field.NameEmptyException;
import ru.t1.ktubaltseva.tm.model.Project;

import java.util.List;

public interface IProjectService extends IUserOwnedService<Project> {

    Project create(String userId, String name, String description) throws AbstractFieldException, AuthRequiredException;

    Project create(String userId, String name) throws NameEmptyException, AuthRequiredException;

    Project changeProjectStatusById(
            String userId,
            String id,
            Status status
    ) throws AbstractException;

    Project changeProjectStatusByIndex(
            String userId,
            Integer index,
            Status status
    ) throws AbstractException;

    List<Project> findAll(String userId, Sort sort) throws AuthRequiredException;

    Project updateById(
            String userId,
            String id,
            String name,
            String description
    ) throws AbstractException;

    Project updateByIndex(
            String userId,
            Integer index,
            String name,
            String description
    ) throws AbstractException;

}
