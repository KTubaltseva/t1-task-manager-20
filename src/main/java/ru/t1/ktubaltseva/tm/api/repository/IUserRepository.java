package ru.t1.ktubaltseva.tm.api.repository;

import ru.t1.ktubaltseva.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    Boolean isLoginExists(String login);

    Boolean isEmailExists(String email);

}
