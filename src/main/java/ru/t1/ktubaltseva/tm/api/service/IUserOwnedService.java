package ru.t1.ktubaltseva.tm.api.service;

import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.AbstractEntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.IndexIncorrectException;
import ru.t1.ktubaltseva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    M add(String userId, M model) throws AbstractEntityNotFoundException, AuthRequiredException;

    void clear(String userId) throws AuthRequiredException;

    List<M> findAll(String userId) throws AuthRequiredException;

    List<M> findAll(String userId, Comparator<M> comparator) throws AuthRequiredException;

    M findOneById(String userId, String id) throws AbstractException;

    M findOneByIndex(String userId, Integer index) throws AbstractException;

    M remove(String userId, M model) throws EntityNotFoundException, AuthRequiredException;

    M removeById(String userId, String id) throws IdEmptyException, AuthRequiredException;

    M removeByIndex(String userId, Integer index) throws IndexIncorrectException, AuthRequiredException;

}
