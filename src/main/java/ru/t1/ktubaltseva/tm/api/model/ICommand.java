package ru.t1.ktubaltseva.tm.api.model;

import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public interface ICommand {
    String getName();

    String getArgument();

    String getDescription();

    Role[] getRoles();

    void execute() throws AbstractException;

    @Override
    String toString();
}
