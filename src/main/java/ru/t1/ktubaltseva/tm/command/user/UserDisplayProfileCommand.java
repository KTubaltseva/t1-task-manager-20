package ru.t1.ktubaltseva.tm.command.user;

import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public class UserDisplayProfileCommand extends AbstractUserCommand {

    private final String NAME = "user-display-profile";

    private final String DESC = "Display user profile.";

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DISPLAY USER PROFILE]");
        displayUser(getAuthService().getUser());
    }

}
