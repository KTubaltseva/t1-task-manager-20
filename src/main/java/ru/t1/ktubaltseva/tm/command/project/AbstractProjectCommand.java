package ru.t1.ktubaltseva.tm.command.project;

import ru.t1.ktubaltseva.tm.api.service.IProjectService;
import ru.t1.ktubaltseva.tm.api.service.IProjectTaskService;
import ru.t1.ktubaltseva.tm.command.AbstractCommand;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.model.Project;

import java.util.List;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void displayProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESC: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }


    protected void renderProjects(final List<Project> projects) {
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            System.out.println();
            index++;
        }
    }

    protected void renderProjectsFullInfo(final List<Project> projects) {
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ".");
            displayProject(project);
            System.out.println();
            index++;
        }
    }

}
