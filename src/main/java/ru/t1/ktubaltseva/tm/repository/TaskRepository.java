package ru.t1.ktubaltseva.tm.repository;

import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task(name, description);
        return add(userId, task);
    }

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task(name);
        return add(userId, task);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        final List<Task> result = new ArrayList<>();
        if (userId == null || userId.isEmpty()) return result;
        if (projectId == null || projectId.isEmpty()) return result;
        for (final Task task : models) {
            if (!userId.equals(task.getUserId())) continue;
            if (!projectId.equals(task.getProjectId())) continue;
            result.add(task);
        }
        return result;
    }

}
