package ru.t1.ktubaltseva.tm.enumerated;

public enum Role {

    USUAL("Usual user"),
    ADMIN("Admin user");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String toString() {
        return getDisplayName();
    }
}
